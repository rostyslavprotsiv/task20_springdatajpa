package com.rostyslavprotsiv.dto;

import com.rostyslavprotsiv.domain.ClientEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class ClientDto extends ResourceSupport {
    ClientEntity client;

    public ClientDto(ClientEntity client, Link selfLink) {
        this.client = client;
        add(selfLink);
        //add(linkTo(methodOn(PersonController.class).getPersonByCityId(city
        // .getId))).withRel("persons"));
    }

    public Integer getClientId() {
        return client.getId();
    }

    public String getName() {
        return client.getName();
    }

    public String getSurname() {
        return client.getSurname();
    }

    public String getMiddleName() {
        return client.getMiddleName();
    }

    public Integer getPackageCount() {
        return client.getPackageCount();
    }
}
