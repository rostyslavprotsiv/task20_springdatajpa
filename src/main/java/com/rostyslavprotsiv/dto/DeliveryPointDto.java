package com.rostyslavprotsiv.dto;

import com.rostyslavprotsiv.domain.DeliveryPointEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class DeliveryPointDto extends ResourceSupport {
    DeliveryPointEntity deliveryPoint;

    public DeliveryPointDto(DeliveryPointEntity deliveryPoint, Link selfLink) {
        this.deliveryPoint = deliveryPoint;
        add(selfLink);
    }

    public Integer getDeliveryPointId() {
        return deliveryPoint.getId();
    }

    public String getAddress() {
        return deliveryPoint.getAddress();
    }

    public String getCity() {
        return deliveryPoint.getCity();
    }

    public String getPointId() {
        return deliveryPoint.getPointId();
    }
}
