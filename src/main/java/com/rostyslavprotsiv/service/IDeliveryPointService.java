package com.rostyslavprotsiv.service;

import com.rostyslavprotsiv.domain.DeliveryPointEntity;
import com.rostyslavprotsiv.exception.DeliveryPointLogicalException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IDeliveryPointService extends
        IGeneralService<DeliveryPointEntity, Integer> {
    List<DeliveryPointEntity> findByCity(String city) throws DeliveryPointLogicalException;
}
