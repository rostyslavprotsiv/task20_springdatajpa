package com.rostyslavprotsiv.service;

import com.rostyslavprotsiv.domain.ClientEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IClientService extends IGeneralService<ClientEntity, Integer> {
    List<ClientEntity> findBySurname(String surname);
}
