package com.rostyslavprotsiv.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IGeneralService<T, ID> {
    T findById(ID id);
    List<T> findAll();
    void deleteById(ID id);
    T create(T entity);
}
