package com.rostyslavprotsiv.service;

import com.rostyslavprotsiv.domain.ClientEntity;
import com.rostyslavprotsiv.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ClientService implements IClientService{
    @Autowired
    ClientRepository clientRepository;

    @Override
    public List<ClientEntity> findAll() {
        return clientRepository.findAll();
    }
    @Override
    public ClientEntity findById(Integer id) {
        return clientRepository.findById(id).get();
    }

    @Transactional
    @Override
    public void deleteById(Integer id) {
        clientRepository.deleteById(id);
    }

    @Override
    public ClientEntity create(ClientEntity entity) {
        return clientRepository.save(entity);
    }

    @Override
    public List<ClientEntity> findBySurname(String surname) {
        return clientRepository.findBySurname(surname);
    }

}
