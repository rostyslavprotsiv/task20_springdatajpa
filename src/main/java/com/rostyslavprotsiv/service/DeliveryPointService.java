package com.rostyslavprotsiv.service;

import com.rostyslavprotsiv.domain.DeliveryPointEntity;
import com.rostyslavprotsiv.exception.DeliveryPointLogicalException;
import com.rostyslavprotsiv.repository.DeliveryPointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class DeliveryPointService implements IDeliveryPointService {
    @Autowired
    DeliveryPointRepository deliveryPointRepository;

    @Override
    public List<DeliveryPointEntity> findAll() {
        return deliveryPointRepository.findAll();
    }

    @Override
    public DeliveryPointEntity findById(Integer id) {
        return deliveryPointRepository.findById(id).get();
    }

    @Override
    public void deleteById(Integer id) {
        deliveryPointRepository.deleteById(id);
    }

    @Transactional
    @Override
    public DeliveryPointEntity create(DeliveryPointEntity entity) {
        return deliveryPointRepository.save(entity);
    }

    @Override
    public List<DeliveryPointEntity> findByCity(String city)
            throws DeliveryPointLogicalException {
        if(city.equals("AAA")) {
            throw new DeliveryPointLogicalException();
        }
        return deliveryPointRepository.findByCity(city);
    }

}
