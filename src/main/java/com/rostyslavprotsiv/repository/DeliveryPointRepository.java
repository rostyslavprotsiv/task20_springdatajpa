package com.rostyslavprotsiv.repository;

import com.rostyslavprotsiv.domain.DeliveryPointEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeliveryPointRepository extends
        JpaRepository<DeliveryPointEntity, Integer> {
    List<DeliveryPointEntity> findByCity(String city);
}
