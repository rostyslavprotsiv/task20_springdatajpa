package com.rostyslavprotsiv.exception;

public class DeliveryPointLogicalException extends Exception {
    public DeliveryPointLogicalException() {
    }

    public DeliveryPointLogicalException(String s) {
        super(s);
    }

    public DeliveryPointLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public DeliveryPointLogicalException(Throwable throwable) {
        super(throwable);
    }

    public DeliveryPointLogicalException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
