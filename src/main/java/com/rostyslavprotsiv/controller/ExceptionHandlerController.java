package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.dto.MessageDto;
import com.rostyslavprotsiv.exception.DeliveryPointLogicalException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(DeliveryPointLogicalException.class)
    ResponseEntity<MessageDto> handleDeliveryPointLogicalExceptio() {
        return new ResponseEntity<>(new MessageDto("City == null"),
                HttpStatus.NOT_FOUND);
    }

}
