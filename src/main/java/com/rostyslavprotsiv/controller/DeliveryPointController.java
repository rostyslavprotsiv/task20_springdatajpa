package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.domain.DeliveryPointEntity;
import com.rostyslavprotsiv.dto.DeliveryPointDto;
import com.rostyslavprotsiv.exception.DeliveryPointLogicalException;
import com.rostyslavprotsiv.service.DeliveryPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class DeliveryPointController {
    @Autowired
    DeliveryPointService deliveryPointService;

    @GetMapping(value = "/api/delivery_point")
    public ResponseEntity<List<DeliveryPointDto>> getAll() {
        List<DeliveryPointEntity> entities = deliveryPointService.findAll();
        Link link = linkTo(methodOn(DeliveryPointController.class)
                .getAll()).withSelfRel();
        List<DeliveryPointDto> deliveryPointDtos = new ArrayList<>();
        for (DeliveryPointEntity entity : entities) {
            Link selfLink = new Link(link.getHref() + "/"
                    + entity.getId()).withSelfRel();
            DeliveryPointDto dto = new DeliveryPointDto(entity, selfLink);
            deliveryPointDtos.add(dto);
        }
        return new ResponseEntity<>(deliveryPointDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/api/delivery_point/{deliveryPointId}")
    public ResponseEntity<DeliveryPointDto> getById(
            @PathVariable Integer deliveryPointId) {
        DeliveryPointEntity entity = deliveryPointService.findById(deliveryPointId);
        Link link = linkTo(methodOn(DeliveryPointController.class)
                .getById(deliveryPointId)).withSelfRel();
        DeliveryPointDto dto = new DeliveryPointDto(entity, link);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping(value = "/api/delivery_point_by_city")
    public ResponseEntity<List<DeliveryPointDto>> getAddressByCity(String city)
            throws DeliveryPointLogicalException {
        List<DeliveryPointEntity> all = deliveryPointService.findByCity(city);
        Link link = linkTo(methodOn(DeliveryPointController.class)
                .getAddressByCity(city)).withSelfRel();
        List<DeliveryPointDto> deliveryPointDtos = new LinkedList<>();
        for (DeliveryPointEntity entity : all) {
            Link selfLink = new Link(link.getHref() + "/"
                    + entity.getId()).withSelfRel();
            DeliveryPointDto dto = new DeliveryPointDto(entity, selfLink);
            deliveryPointDtos.add(dto);
        }
        return new ResponseEntity<>(deliveryPointDtos , HttpStatus.OK);
    }

    @PostMapping(value = "/api/delivery_point")
    public ResponseEntity<DeliveryPointDto> addDeliveryPoint(
            @RequestBody DeliveryPointEntity entity) {
        deliveryPointService.create(entity);
        Link link = linkTo(methodOn(DeliveryPointController.class).getById(entity.getId())).withSelfRel();
        DeliveryPointDto dto = new DeliveryPointDto(entity, link);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/delivery_point/{id}")
    public ResponseEntity deleteById(
            @PathVariable Integer id) {
        deliveryPointService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

}
