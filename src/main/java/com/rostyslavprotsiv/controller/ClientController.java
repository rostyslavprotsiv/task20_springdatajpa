package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.domain.ClientEntity;
import com.rostyslavprotsiv.dto.ClientDto;
import com.rostyslavprotsiv.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class ClientController {
    @Autowired
    ClientService clientService;

    @GetMapping(value = "/api/client")
    public ResponseEntity<List<ClientDto>> getAllClients() {
        List<ClientEntity> clientEntities = clientService.findAll();
        Link link = linkTo(methodOn(ClientController.class).getAllClients())
                .withSelfRel();
        List<ClientDto> clientDtos = new ArrayList<>();
        for (ClientEntity entity : clientEntities) {
            Link selfLink = new Link(link.getHref() + "/" + entity.getId())
                    .withSelfRel();
            ClientDto dto = new ClientDto(entity, selfLink);
            clientDtos.add(dto);
        }
        return new ResponseEntity<>(clientDtos, HttpStatus.OK);
    }

    @GetMapping(value = "/api/client/{clientId}")
    public ResponseEntity<ClientDto> getClientById(@PathVariable Integer clientId) {
        ClientEntity entity = clientService.findById(clientId);
        Link link = linkTo(methodOn(ClientController.class)
                .getClientById(clientId)).withSelfRel();
        ClientDto dto = new ClientDto(entity, link);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @DeleteMapping(value = "/api/client/{clientId}")
    public ResponseEntity deleteClientById(@PathVariable Integer clientId) {
        clientService.deleteById(clientId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @PostMapping(value = "/api/client")
    public ResponseEntity<ClientDto> addClient(@RequestBody ClientEntity newClient) {
        clientService.create(newClient);
        Link link = linkTo(methodOn(ClientController.class)
                .getClientById(newClient.getId())).withSelfRel();
        ClientDto dto = new ClientDto(newClient, link);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @GetMapping(value = "/api/client/")
    public ResponseEntity<List<ClientDto>> findBySurname(String surname) {
        List<ClientEntity> clientEntities = clientService
                .findBySurname(surname);
        Link link = linkTo(methodOn(ClientController.class)
                .findBySurname(surname)).withSelfRel();
        List<ClientDto> clientDtos = new ArrayList<>();
        for (ClientEntity entity : clientEntities) {
            Link selfLink = new Link(link.getHref() +
                    entity.getId()).withSelfRel();
            ClientDto dto = new ClientDto(entity, selfLink);
            clientDtos.add(dto);
        }
        return new ResponseEntity<>(clientDtos, HttpStatus.OK);
    }
}
